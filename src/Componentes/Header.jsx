import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";

function Header({ encabezado }) {
  return (
    <>
      <style></style>
      <Navbar expand="lg" className="bg-body-tertiary">
        <Container>
          <Navbar.Brand href="#">{encabezado}</Navbar.Brand>
        </Container>
      </Navbar>
    </>
  );
}

export default Header;
